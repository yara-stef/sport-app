import {   
    signInWithEmailAndPassword,
    signOut,
} from 'firebase/auth';
import { auth } from "./firebase";



const logInWithEmailAndPassword = async ({email, password}) => {
  try {
    const user = await signInWithEmailAndPassword(auth, email, password);
    return user;
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};


const logout = async () => {
 await signOut(auth);
};
export {
  logInWithEmailAndPassword,
  logout,
};