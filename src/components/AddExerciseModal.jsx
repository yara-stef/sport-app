import React, { useState } from 'react';
import { useExercises } from '../hooks/useExercises';

function AddExerciseModal({ isVisible, onClose, children }) {
    const [exerciseName, setExerciseName] = useState('');
    const [description, setDescription] = useState('');
    const [muscleGroup, setMuscleGroup] = useState('');
    const [equipmentNeeded, setEquipmentNeeded] = useState('');
    const [difficulty, setDifficulty] = useState('');
    const { addExercise } = useExercises();

    if(!isVisible) return null;

    const handleClose = (e) => {
        if(e.target.id === 'wrapper') onClose();
    }

    const handleChange = (e) => {
        setExerciseName(e.target.value);
    }

    const handleClick = () => {
        addExercise({
            name: exerciseName, 
            description, 
            muscleGroup, 
            equipmentNeeded, 
            difficulty,
        });
        onClose();
    }

  return (
    <div className='fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center' id='wrapper' onClick={handleClose}>
        <div className='md:w-[800px] w-[90%] mx-auto flex flex-col'>
            <button className='text-white text-xl place-self-end' onClick={() => onClose()} >X</button>
            <div className='bd-zinc-800 p-2 rounded'>
                {children}
                <div className='pb-2 pl-1 text-3xl'>Add an exercise
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Exercise name'  value={exerciseName} onChange={handleChange}>
                    </input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Description'  value={description} onChange={(e) => setDescription(e.target.value)}>
                    </input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Muscle group'  value={muscleGroup} onChange={(e) => setMuscleGroup(e.target.value)}>
                    </input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Equipment'  value={equipmentNeeded} onChange={(e) => setEquipmentNeeded(e.target.value)}>
                    </input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Difficulty'  value={difficulty} onChange={(e) => setDifficulty(e.target.value)}>
                    </input>
                </div>
                <button  className="mt-2 w-64 h-10 bg-zinc-800 rounded shadow-2xl shadow-cyan-500/80 hover:shadow-indigo-500/80" onClick={handleClick}>Add</button>                
            </div>
        </div>
    </div>
  )
}

export default AddExerciseModal