import React, { useState } from 'react';
import { useAthletes } from '../hooks/useAthletes';
import { useExercises } from '../hooks/useExercises';
import Search from '../features/Search';
import ExercisesList from '../components/ExercisesList';

function AddWorkoutModal({ isVisible, onClose, children }) {
    const [exercise, setExercise] = useState('');
    const [sets, setSets] = useState();
    const [reps, setReps] = useState();
    const [weight, setWeight] = useState();
    const { addWorkout } = useAthletes();
    const { exercises } = useExercises();
    const [exercisesList, setExercisesList] = useState(true);
    // console.log(exercises);



    if(!isVisible) return null;

    const handleClose = (e) => {
        if(e.target.id === 'wrapper') onClose();
    }

    const handleClick = () => {
        addWorkout({exercise, sets, reps, weight});
        onClose();
    }

    const keys = ["name"];
    const search = ( data) => {
        console.log(data);
        return data.filter((item) => 
        keys.some((key) => item[key].toLowerCase().includes(exercise)))
    }

  return (
    <div className='fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center' id='wrapper' onClick={handleClose}>
        <div className='md:w-[800px] w-[90%] mx-auto flex flex-col'>
            <button className='text-white text-xl place-self-end' onClick={() => onClose()} >X</button>
            <div className='bd-zinc-800 p-2 rounded'>
                {children}
                <div className='pb-2 pl-1 text-3xl'>Select an exercise
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40' onChange={() => search(exercises)}>
                    <Search setSearchName={setExercise} />
                    <ExercisesList isVisible={exercisesList} exercises={search(exercises)} />
                    
                    {/* <input className='text-black rounded' placeholder='Exercise name'  value={exercise} onChange={(e) => setExercise(e.target.value)}>
                    </input> */}
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Sets' type='Number' min={1} max={10}  value={sets} onChange={(e) => setSets(e.target.value)}>
                    </input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Reps' type='Number' min={1} max={30} value={reps} onChange={(e) => setReps(e.target.value)}>
                    </input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Weight' type='Number' min={1} value={weight} onChange={(e) => setWeight(e.target.value)}>
                    </input>
                </div>
                <button  className="mt-2 w-64 h-10 bg-zinc-800 rounded shadow-2xl shadow-cyan-500/80 hover:shadow-indigo-500/80" onClick={handleClick}>Add Workout</button>       
            </div>
        </div>
    </div>
  )
}

export default AddWorkoutModal