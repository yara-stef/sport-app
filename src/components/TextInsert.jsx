import React, { useState } from 'react';
import { useAthletes } from '../hooks/useAthletes';

function TextInsert({ isVisible, athlete }) {

    const [text, setText] = useState('');
    const [textDisplay, setTextDisplay] = useState([]);
    const { addAthlete, athletes } = useAthletes();

    // console.log(athlete);

    if(!isVisible) return null;
   
    const handleSubmit = (event) => {
        event.preventDefault();
        // console.log(text);
        const data = { text }; 
        if(text) {
            setTextDisplay((textString) => [...textString, data])
            setText('');

        }
        
    }
  return (
    <>
    <form className='pl-5' onSubmit={handleSubmit}>
        <input className='text-black' type='text' value={text} onChange={(e) => setText(e.target.value)}></input>
        <button className='pl-2 hover:bg-black pr-2'>Add</button>
    
    </form>
    {/* {
        textDisplay.map((item, index) =>
        <div key={index}>
            <li>{item.text}</li>
        </div>)
    } */}

    </>
  )
}

export default TextInsert