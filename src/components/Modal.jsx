import React from 'react';
import Player from './Player';

function Modal({ isVisible, onClose, children }) {
    if(!isVisible) return null;

    const handleClose = (e) => {
        if(e.target.id === 'wrapper') onClose();
    }

  return (
    <div className='fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center' id='wrapper' onClick={handleClose}>
        <div className='md:w-[800px] w-[90%] mx-auto flex flex-col'>
            <button className='text-white text-xl place-self-end' onClick={() => onClose()} >X</button>
            <div className='bd-zinc-800 p-2 rounded'>
                {children}
                <Player />
                {/* <iframe width="100%" height="315" src="https://www.youtube.com/embed/cI_gFJ_9i-Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> */}
                </div>
        </div>
    </div>
  )
}

export default Modal