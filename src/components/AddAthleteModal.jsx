import React, { useRef, useState } from 'react';
import { useAthletes } from '../hooks/useAthletes';

function AddAthleteModal({ isVisible, onClose, children }) {
    const [athleteName, setAthleteName] = useState('');
    const [age, setAge] = useState('');
    const [weight, setWeight] = useState('');
    const [experience, setExperience] = useState('');
    const [date, setDate] = useState('');
    const [gender, setGender] = useState('');
    const [healthCondition, setHealthCondition] = useState('');
    const [goals, setGoals] = useState([]);
    const dateInputRef = useRef(null);
    const { addAthlete } = useAthletes();
    // const genders = ['Male', 'Female', 'Other'];


    if(!isVisible) return null;

    const handleClose = (e) => {
        if(e.target.id === 'wrapper') onClose();
    }

    const handleClick = () => {
        console.log(`You are trying to add a new client ${athleteName}`);
        // setathleteName('');
        addAthlete({ name: athleteName, age, weight, experience, birthdate: date, gender, healthCondition, goals });
        onClose();
    }

    const handleChange = (e) => {
        setAthleteName(e.target.value);
        
    }

    const handleDateChange = (e) => {
        setDate(e.target.value);
    }

    // const onOptionChangeHandler = (e) => {
    //     console.log(e.target.value);
    // }

  return (
    <div className='fixed inset-0 bg-black bg-opacity-25 backdrop-blur-sm flex justify-center items-center' id='wrapper' onClick={handleClose}>
        <div className='md:w-[800px] w-[90%] mx-auto flex flex-col'>
            <button className='text-white text-xl place-self-end' onClick={() => onClose()} >X</button>
            <div className='bd-zinc-800 p-2 rounded'>
                {children}
                <div className='pb-2 pl-1 text-3xl'>Add An Athlete</div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'><input className='text-black rounded' placeholder='Name'  value={athleteName} onChange={handleChange}></input></div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'><input className='text-black rounded' placeholder='Age' type='Number' min='14' max='90' value={age} onChange={(e) => setAge(e.target.value)}></input></div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'><input className='text-black rounded' placeholder='Weight' type='Number' min='40' value={weight} onChange={(e) => setWeight(e.target.value)}></input></div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'><input className='text-black rounded' placeholder='Experience' type='Number' min='0' value={experience} onChange={(e) => setExperience(e.target.value)}></input></div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <input className='text-black rounded' placeholder='Gender' value={gender} onChange={(e) => setGender(e.target.value)}></input>
                    {/* <option>Please choose gender</option>
                    
                    {genders.map((gender, index) => {
                        return                       <option key={index} onChange={() => addAthlete({gender: gender})}>{gender}</option>
                    })} */}
                    {/* <option value='male' >Male</option>
                    <option value='female' >Female</option>
                    <option value='other' >Other</option> */}
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'><input className='text-black rounded' type='date' onChange={handleDateChange} ref={dateInputRef}></input></div>
                {/* <form  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                    <label htmlFor='birthday'>Birthday:</label>&nbsp;
                    <input className='text-black rounded'   type='date' id='birthday' name='birthday' ></input>
                </form> */}
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                <input className='text-black rounded' placeholder='Health condition' value={healthCondition} onChange={(e) => setHealthCondition(e.target.value)}></input>
                </div>
                <div  className='border-solid border-2 p-4 rounded shadow-2xl shadow-cyan-500/40 hover:shadow-indigo-500/40'>
                <input className='text-black rounded' placeholder='Goals' value={goals} onChange={(e) => setGoals(e.target.value)}></input>
                </div>
                <button  className="mt-2 w-64 h-10 bg-zinc-800 rounded shadow-2xl shadow-cyan-500/80 hover:shadow-indigo-500/80" onClick={handleClick}>Add</button>
                </div>
                
            </div>
        </div>
    
  )
}

export default AddAthleteModal