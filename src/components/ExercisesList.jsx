import React, { useState } from 'react';
import { useExercises } from '../hooks/useExercises';
import { useAthletes } from '../hooks/useAthletes';

function ExercisesList({ isVisible, exercises }) {
    // const { exercises } = useExercises();
    const { addWorkout } = useAthletes();

    if(!isVisible) return null;

  const handleClick = () => {
    addWorkout({})
  }

  return (
    <ul>
        {exercises.map((exercise, index) => {
            return (
                <li key={index} onClick={handleClick}>{exercise.name}</li>)
        })}
    </ul>
  )
}

export default ExercisesList