import React, { useState } from 'react';
import TextInsert from '../components/TextInsert';
import { useSelector } from 'react-redux';
import editIcon from '../img/edit-icon.png';

function ChangeInfo() {
  const [showInput, setShowInput] = useState(false);
  const athlete = useSelector(state => state.athletes.chosenAthlete);

  // console.log(athlete);

    const handleClick = () => {
        console.log("You clicked");
        setShowInput(true);
    }

  return (
    <span>
        &nbsp;<button onClick={handleClick}><img src={editIcon}></img> </button>
        <TextInsert isVisible={showInput} athlete={athlete} />
    </span>
  )
}

export default ChangeInfo