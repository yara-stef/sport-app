import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { doc, getDoc } from "firebase/firestore"; 
import {db} from '../services/firebase';

export function useAthlete(id) {
  // const athlete =  useSelector(state => state.athletes.chosenAthlete);
  const dataFromRedux =  useSelector(state => state.athletes.chosenAthlete);
    const [athlete, setAthlete]  = useState(null);

    useEffect (() => { 

      const getAthletData = async (athleteId) => {
        try {
          debugger
            const athleteRef = doc(db, "athletes", athleteId);
            const athletSnap = await getDoc(athleteRef);
            debugger
            if (athletSnap.exists()) {
              
              return { id: athletSnap.id, ...athletSnap.data() }
            } else {
              throw new Error("No such document!");
            }
        } catch (error) {
            throw new Error(error);
        }
      }    
      
if(dataFromRedux) {
  setAthlete(dataFromRedux)
  return
}
debugger
      const getAthlete = async () => {
        const athlete = await getAthletData(id);
        debugger
        setAthlete(athlete);
      };
      
      getAthlete();
        // const dataFromStore = getAthletDataFromStore();
        // const fetchData = async () => {
        //     const dataFromFirebase = await getAthletData(id);
        //     setAthlete(dataFromFirebase);
        // };
        // if (!dataFromStore) {
        //     fetchData();
        // };
        // return setAthlete(dataFromStore);
              
    }, [dataFromRedux]);

    // const getAthletDataFromStore = () => {
    //     return useSelector(state => state.athletes.chosenAthlete);
    // }    

    
      
    
    // debugger
  return {
    athlete
  };
}

