import React from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import AthleteInfo from '../features/AthleteInfo';
import TrainingPlan from '../features/TrainingPlan';
import Diet from '../features/Diet';
import Links from '../features/Links';
import { useAthletes } from '../hooks/useAthletes';
import { useSelector } from 'react-redux';
import { useAthlete } from '../hooks/useAthlete';
import { doc, getDoc } from "firebase/firestore"; 
import {db} from '../services/firebase';

function SingleAthlete() {
    const { id } = useParams();
    const navigate = useNavigate();
    const { athlete } = useAthlete(id);
    // debugger
//     let athlete = useSelector(state => state.athletes.chosenAthlete);

// console.log(athlete);

//     const getAthletData = async (athleteId) => {
//       try {
//           const athleteRef = doc(db, "athletes", athleteId);
//           const athletSnap = await getDoc(athleteRef);
//           if (athletSnap.exists()) {
//             return { id: athletSnap.id, ...athletSnap.data() }
//           } else {
//             throw new Error("No such document!");
//           }
//       } catch (error) {
//           throw new Error(error);
//       }
//     }

//    const fetchData = async () => {
//          const dataFromFirebase = await getAthletData(id);
         
//           athlete = dataFromFirebase;
//           console.log(athlete);
//           return athlete;
//      };
  
//   if (!athlete) {    
//        fetchData();
//   };

    
    

    // const { athletes } = useAthletes(id);
    // console.log(id);

    // const client =  athletes.find(athlete => athlete.id === id);

    
  return (
    <>
    <div className='pt-5 text-center text-2xl font-semibold flex justify-center'>
      <div className=' w-1/4 h-15 pb-5 '></div>
    </div>
    <div className="grid lg:grid-cols-2 gap-4 ">
        <div className='mr-2 px-4 pt-5 lg:grid lg:grid-cols-2 gap-4'>
            <AthleteInfo athlete={athlete} />
            <TrainingPlan />
            <Diet />
        </div>
        <div className='pt-5 px-4 mr-2'>
          <Links />
        </div>
        <button className='text-start m-5 bg-zinc-800 w-1/4 rounded hover:bg-black' onClick={() => navigate('/')}>← Return</button>
    </div>
    </>
    
  )
}

export default SingleAthlete