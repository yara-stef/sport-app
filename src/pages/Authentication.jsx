import React from 'react';
import AuthenticationImage from '../features/AuthenticationImage';
import AuthenticationLinks from '../features/AuthenticationLinks';
import AuthenticationName from '../features/AuthenticationName';

function Authentication() {
  return (
   
        <div>
          <div className='h-[800px] flex justify-center'>
            <div className='py-16 bg-zink-800 flex flex-col justify-center'>
              <AuthenticationImage />
            <AuthenticationName />
            <AuthenticationLinks />
            </div>
            
          </div>
             
        </div>
        
   
  )
}

export default Authentication