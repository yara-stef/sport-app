import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import TextInsert from '../components/TextInsert';
import ChangeInfo from '../components/ChangeInfo';

function AthleteInfo({ athlete }) {
  const { id } = useParams();
  const [isShown, setIsShown] = useState(false);
   
  // console.log(athlete);
  
 if(!athlete) {
  return null;
 }

//  console.log(athlete.birthdate);

  return (
    <>
    
        <div className='bg-zinc-800 col-span-2 pb-5 mb-8 shadow-2xl shadow-cyan-500/50 rounded hover:shadow-indigo-500/40'>
            <h2 className='text-center pt-5 pb-5 text-xl'>Athlete information:</h2>
            <table className='border-collapse border border-slate-400 ml-5 mr-5 w-11/12'>
              <tbody>
                <tr>
                  <td className='border border-slate-300 pl-5'>Fullname:</td>
                  <td className='border border-slate-300 pl-5' onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>                  
                    {athlete.name}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                    )} </td>
                    
                    
                   
                </tr>
                <tr>
                  <td className='border border-slate-300 pl-5'>Age:</td>
                  <td className='border border-slate-300 pl-5' onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>{athlete.age}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                    )}</td>
                </tr>
                <tr>
                  <td className='border border-slate-300 pl-5'>Gender:</td>
                  <td className='border border-slate-300 pl-5' onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>{athlete.gender}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                    )}</td>
                </tr>
                <tr>
                  <td className='border border-slate-300 pl-5'>Training experience:</td>
                  <td className='border border-slate-300 pl-5'  onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>
                     {athlete.experience}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                    )}
                  </td>
                </tr>
                <tr>
                  <td className='border border-slate-300 pl-5'>Birthdate:</td>
                  <td className='border border-slate-300 pl-5' onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>
                     {/* {athlete.birthdate} */}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                      )}
                  </td>
                </tr>
                <tr>
                  <td className='border border-slate-300 pl-5'>Health condition:</td>
                  <td className='border border-slate-300' onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>
                    {athlete.healthCondition}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                    )}
                    {/* <TextInsert /> */}
                    </td>
                </tr>
                <tr>
                  <td className='border border-slate-300 pl-5'>Goals:</td>
                  <td className='border border-slate-300' onMouseEnter={() => setIsShown(true)} onMouseLeave={() => setIsShown(false)}>
                    {athlete.goals}{isShown && (                     
                      <span><ChangeInfo /></span>
                                            
                    )}
                    {/* <TextInsert /> */}
                    </td>
                </tr>
              </tbody>
            </table>
            <h2 className='text-center pt-5 pb-5 text-xl'>Progress: </h2>  
            
            <TextInsert /> 
        </div>
        
    </>
  )
}

export default AthleteInfo