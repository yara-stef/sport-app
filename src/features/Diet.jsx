import React, { useState } from 'react';
import TextInsert from '../components/TextInsert';

function Diet() {
    
       
  return (
    <>
        <div className='mt-8 lg:mt-0 pb-4 bg-zinc-800 shadow-2xl shadow-cyan-500/50 rounded hover:shadow-indigo-500/40'>
            <h2 className='text-center pt-5 pb-5 text-xl'>Diet recommendations:</h2>
            <TextInsert />
        </div>  
    </>
  )
}

export default Diet