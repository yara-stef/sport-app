import React, { useState } from 'react';
import { useExercises } from '../hooks/useExercises';
import AddExerciseModal from '../components/AddExerciseModal';
import AddWorkoutModal from '../components/AddWorkoutModal';
import TextInsert from '../components/TextInsert';

function TrainingPlan() {
  const [exercise, setExercise] = useState('');
  const [showModal1, setShowModal1] = useState(false);
  const [showModal2, setShowModal2] = useState(false);
  const { addExercise } = useExercises();

  const handleChange = (e) => {
    setExercise(e.target.value);
  }

  const handleClick = () => {
    console.log(exercise);
    addExercise({exercise});
  }

  return (
    <>
    <div className='bg-zinc-800 pb-5 shadow-2xl shadow-cyan-500/50 rounded hover:shadow-indigo-500/40'>
        <h2 className='text-center pt-5 pb-5 text-xl'>Training plan:</h2>
        <table className='border-collapse border border-slate-400 ml-5 mr-5  w-10/12'>
          <thead>
            <tr>
              <td className='border border-slate-300 pl-5'>Day 1</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='border border-slate-300'>
                <div className=''>
                  <button className="w-48 h-10 bg-zinc-800 rounded shadow-2xl shadow-cyan-500/80 hover:shadow-indigo-500/80" onClick={() => setShowModal1(true)}>Add Exercise
                  </button>
                </div>
                <div className=''>
                  <button className="w-48 h-10 bg-zinc-800 rounded shadow-2xl shadow-cyan-500/80 hover:shadow-indigo-500/80" onClick={() => setShowModal2(true)}>Select Exercise
                  </button>
                </div>
                {/* <input className='text-black rounded' placeholder='Add Exercise' value={exercise} onChange={handleChange}></input>
                <button className='pl-2 hover:bg-black pr-2' onClick={handleClick}>Add</button> */}
                {/* <TextInsert /> */}
              </td>
            </tr>
          </tbody>
          <thead>
            <tr>
              <td className='border border-slate-300 pl-5'>Day 2</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='border border-slate-300'>
                {/* <TextInsert /> */}
                </td>
            </tr>
          </tbody>
          <thead>
            <tr>
              <td className='border border-slate-300 pl-5'>Day 3</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='border border-slate-300'>
                {/* <TextInsert /> */}
                </td>
            </tr>
          </tbody>
          <thead>
            <tr>
              <td className='border border-slate-300 pl-5'>Day 4</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className='border border-slate-300'>
                {/* <TextInsert /> */}
                </td>
            </tr>
          </tbody>
        </table>
        {/* <div className='pl-5 pb-5'>
            <p>Day 1</p>
            <p>Day 2</p>
            <p>Day 3</p>
            <p>Day 4</p>
        </div>         */}
    </div>  
    <AddExerciseModal isVisible={showModal1} onClose={() => setShowModal1(false)} />      
    <AddWorkoutModal isVisible={showModal2} onClose={() => setShowModal2(false)} />      
    </>
    
  )
}

export default TrainingPlan