import React, { Fragment, useState } from 'react';
import Modal from '../components/Modal';

function Links() {

  const [showModal, setShowModal] = useState(false);
  const [showModal1, setShowModal1] = useState(false);

  return (
    <>
    <Fragment>
      <div className='px-4 lg:ml-0 bg-zinc-800 shadow-2xl shadow-cyan-500/50 rounded hover:shadow-indigo-500/40 pb-5'>
            <h2 className='text-center pt-5 pb-5 text-xl'>Usefull links and materials:</h2>
            <div className='pl-5'>
              <a className='cursor-pointer' onClick={() => setShowModal(true)}>Video 1</a>
            </div>
            <div className='pl-5'> 
              <a className='cursor-pointer' onClick={() => setShowModal1(true)}>Video 2</a>
            </div>
            <div className='pl-5'>
              <a className='cursor-pointer' >Video 3</a>
            </div>
            <div className='pl-5'>
              <a className='cursor-pointer' >Video 4</a>
            </div>
            <Modal isVisible={showModal} onClose={() => setShowModal(false)} />
            
            <Modal isVisible={showModal1} onClose={() => setShowModal1(false)} />

            {/*instagram token: IGQVJXNjNxc09GUy05UnR4dFA1cVJxWjl5ZAVFOSXpoQ2VScGZAma2VYN29OdDVZAWjM2UnZArLVlLY251VWlUalV3U3kyaWxQbjZAULWV0UGRCTlE4UnBvdFVyajU2dmczZAFI4eDVmZAzdqS2Jvb0Q2YVlQLQZDZD */}
        </div>
    </Fragment>
        
        
    </>
  )
}

export default Links